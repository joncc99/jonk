// astro.config.mjs
import { defineConfig } from 'astro/config';

// https://astro.build/config
export default defineConfig({
    // GitLab Pages requires exposed files to be located in a folder called "public".
    // So we're instructing Astro to put the static build output in a folder of that name.
    outDir: 'public',

    // The folder name Astro uses for static files (`public`) is already reserved
    // for the build output. So in deviation from the defaults we're using a folder
    // called `static` instead.
    publicDir: 'static',

    // In case the project is owned by a subgroup, use:
    // base: '/<subgroup>/<project-name>',
    base: '',

    // Note: Instead of specifying both `base` and `site`, you can simply
    // use the full URL here:
    // site: 'https://<user-or-group>.gitlab.io/<project-name>'
    // or for pages owned by a subgroup:
    // site: 'https://<group>.gitlab.io/<subgroup>/<project-name>'
    site: 'https:/joncc99.gitlab.io/'

});